# Sailfish OS on Oneplus One X (onyx)

![SailfishOS](https://sailfishos.org/wp-content/themes/sailfishos/icons/apple-touch-icon-120x120.png)

## Current Build
3.4 - [![pipeline status](https://gitlab.com/sailfishos-porters-ci/onyx-ci/badges/master/pipeline.svg)](https://gitlab.com/sailfishos-porters-ci/onyx-ci/-/jobs)
